import turtle
from math import sqrt

print("test")


# helper
def save_pos():
    turty = dict()
    turty["pos"] = turtle.pos()
    turty["heading"] = turtle.heading()
    return turty


# helper
def restore_pos(turty):
    turtle.penup()
    turtle.setpos(turty["pos"])
    turtle.seth(turty["heading"])
    turtle.pendown()


# replacment tree 1
def branch_left(lvl, max):
    if lvl == max:
        return
    else:
        lvl += 1
        # move turtle
        turtle.left(25)
        turtle.forward(50)

        # save position
        turty = save_pos()

        # continue left branch
        branch_left(lvl, max)

        # set this position
        restore_pos(turty)

        # continue right branch
        branch_right(lvl, max)


# replacement tree 2
def branch_right(lvl, max):
    if lvl == max:
        return
    else:
        lvl += 1
        # move turtle
        turtle.right(25)
        turtle.forward(50)

        # save position
        turty = save_pos()

        # continue left branch
        branch_left(lvl, max)

        # set this position
        restore_pos(turty)

        # continue right branch
        branch_right(lvl, max)


# start word
def tree(max, lvl=0):
    turtle.forward(50)
    turty = save_pos()
    branch_left(lvl, max)
    restore_pos(turty)
    branch_right(lvl, max)


# replacement word
def snowborder(maxlvl, lvl=0):
    if lvl == maxlvl:
        turtle.forward(400/(3**lvl))
    else:
        lvl += 1
        snowborder(maxlvl, lvl)
        turtle.left(60)
        snowborder(maxlvl, lvl)
        turtle.right(120)
        snowborder(maxlvl, lvl)
        turtle.left(60)
        snowborder(maxlvl, lvl)


# startword
def snowflake(maxlvl):
        lvl = 0
        snowborder(maxlvl, lvl)
        turtle.right(120)

        snowborder(maxlvl, lvl)
        turtle.right(120)

        snowborder(maxlvl, lvl)


def dragon_left(maxlvl, lvl=0):
    if lvl == maxlvl:
        turtle.forward(300/(sqrt(2)**lvl))
    else:
        lvl += 1
        turtle.right(45)
        dragon_right(maxlvl, lvl)
        turtle.left(90)
        dragon_left(maxlvl, lvl)
        turtle.right(45)

def dragon_right(maxlvl, lvl=0):
    if lvl == maxlvl:
        turtle.forward(300 / (sqrt(2) ** lvl))
    else:
        lvl += 1
        turtle.left(45)
        dragon_right(maxlvl, lvl)
        turtle.right(90)
        dragon_left(maxlvl, lvl)
        turtle.left(45)

def dragon(maxlvl):
    dragon_left(maxlvl)

def lindenmayer(symbols):

    if "start" not in symbols:
        print("no start word defined. exit.")
        return

    for word in symbols["start"]:
        for instr in word:
            if instr == "+":
                print("turn left")
            elif instr == "-":
                print("turn right")
            elif instr in symbols:
                print("a replacement word")



# tree(3)

# snowflake(5)

dragon(10)

turtle.done()