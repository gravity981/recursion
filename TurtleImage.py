from TurtleBase import *

from PIL import Image, ImageDraw

import math


class TurtleImage(TurtleBase):

    def __init__(self, pos, head):
        self.image = Image.new("RGB", (800, 800), "white")
        self.draw = ImageDraw.Draw(self.image)
        self.penActive = True
        super(TurtleImage, self).__init__(pos, head)

    def left(self, angle):
        self.head += angle
        #self.draw.line((0, 0) + self.image.size, fill=128)

    def right(self, angle):
        self.head -= angle
        #self.draw.line((0, self.image.size[1], self.image.size[0], 0), fill=128)

    def forward(self, distance):
        offset_abs = (self.image.size[0]/2, self.image.size[1]/2)
        startpos = (self.pos[0] + offset_abs[0], offset_abs[1] - self.pos[1])
        angle = math.radians(self.head)

        dx = distance * math.cos(angle)
        dy = -distance * math.sin(angle)

        endpos = tuple(map(sum, zip(startpos, (dx, dy))))
        '''
        print("distance: " + str(distance) + " / dx: " + str(dx) + " / dy: " + str(dy))
        print("pos: " + str(tuple(self.pos)))
        print("offset: " + str(offset_abs))
        print("startpos: " + str(startpos))
        print("endpos: " + str(endpos))
        '''

        if self.penActive:
            self.draw.line(startpos + endpos, fill=128)

        self.pos = (endpos[0]-offset_abs[0], offset_abs[1] - endpos[1])

        # print("pos save: " + str(tuple(self.pos)))


    def position(self):
        return self.pos

    def heading(self):
        return self.head

    def setpos(self, pos):
        self.pos = pos

    def seth(self, heading):
        self.head = heading

    def penup(self):
        self.penActive = False

    def pendown(self):
        self.penActive = True

    def done(self):
        self.image.save("turtle_test.png")