# README #



### About Recursion ###

This project is about playing with recursion using python with turtle.

[Lindenmayer-Systems](https://en.wikipedia.org/wiki/L-system) are parsed from fractals.json

class Lindenmayer uses turtle graphics to draw system read from fractals.json

### Build & Run ###

You need a Python 3 installation
and the following modules
* `pip install Pillow`
* `pip install Django` (tested version: 2.0.2)

To run the webserver go to directory
`django/mysite/` and execute the following
* `py manage.py runserver`
* [http://localhost:8000/recursion/]()


### Todo ###

* clean up files and gitignore
* fix admin section
