from TurtleGraphics import *
from TurtleImage import *
import json

# some test with git 

class Lindenmayer:

    # public
    def __init__(self, turty, words, angle, length, ratio, recursion_lvl, pen_color=None, fill_color=None):
        self.turty = turty
        self.words = words
        self.angle = angle
        self.length = length
        self.ratio = ratio
        self.recursion_lvl = recursion_lvl
        self.pen_color = pen_color
        self.fill_color = fill_color
        self.pos_stack = []

    def start(self):
        if "start" not in self.words:
            print("no start word found")
            return
        else:
            if self.pen_color is not None:
                self.turty.pencolor(self.pen_color)
            '''
            if self.fill_color is not None:
                turty.fillcolor(self.fill_color)
                turty.begin_fill()
            '''

            self.run(self.words["start"], "start")

            '''
            if self.fill_color is not None:
                self.turty.end_fill()
            '''

        self.turty.done()

    # private
    def run(self, word, msg, lvl=0):

        # print(msg + " len: " + str(len(word)) + " lvl: " + str(lvl))

        for symbol in word:
            # print(msg + ": " + symbol)
            if symbol == "+":
                # print(msg + " turn left")
                self.turty.left(self.angle)
            elif symbol == "-":
                # print(msg + " turn right")
                self.turty.right(self.angle)
            elif symbol == "[":
                # print("save turty pos and heading")
                turty_save = dict()
                turty_save["pos"] = self.turty.position()
                turty_save["heading"] = self.turty.heading()
                self.pos_stack.append(turty_save)
            elif symbol == "]":
                # print("restore turty pos and heading")
                turty_save = self.pos_stack.pop()
                self.turty.penup()
                self.turty.setpos(turty_save["pos"])
                self.turty.seth(turty_save["heading"])
                self.turty.pendown()
            elif symbol in word:
                # print("a replacement word")
                if lvl == self.recursion_lvl:
                    # print(msg + " forward")
                    self.turty.forward(self.length*(self.ratio**lvl))
                elif symbol in self.words:
                    self.run(self.words[symbol], symbol, lvl + 1)
            else:
                print("unknown symbol")

        # print("done")

    @staticmethod
    def parseFile(filename, turty):

        jdict = dict()

        # parse json file
        with open(filename, "r") as f:
            jdict = json.load(f)

        if "fractals" not in jdict:
            print("no fractal definition found")
            return None

        jdict = jdict["fractals"][0]

        # some checks
        if "name" not in jdict:
            print("no name definition found")
            return None

        if "lsystem" not in jdict:
            print("no system definition found")
            return None

        if "params" not in jdict:
            print("no params definition found")
            return None

        print(jdict["name"])

        # create words dict
        words = dict()
        for k,v in jdict["lsystem"].items():
            words[k] = list(v)

        params = jdict["params"]

        if "angle" not in params:
            print("angle not defined")
            return None

        if "length" not in params:
            print("length not defined")
            return None

        if "ratio" not in params:
            print("ratio not defined")
            return None

        if "recursion_lvl" not in params:
            print("recursion_lvl not defined")
            return None

        if "pen_color" not in params:
            params["pen_color"] = None

        if "fill_color" not in params:
            params["fill_color"] = None

        if "init_pos" in params:
            pos = params["init_pos"]
            turty.penup()
            turty.setpos(pos)
            turty.pendown()

        if "init_heading" in params:
            turty.seth(params["init_heading"])

        return Lindenmayer(turty, words, params["angle"], params["length"], params["ratio"], params["recursion_lvl"], params["pen_color"], params["fill_color"])

turty = TurtleImage((0,0), 0)
lm = Lindenmayer.parseFile("fractals.json", turty)
if lm is not None:
    lm.start()

'''
todo add feature "[" & "]" save and restore turtle pos/heading (required for trees)

todo select the fractal to draw with some kind of gui

todo modify settings directly from the gui

todo save settings

todo create a fractal systems within gui and save to file
'''