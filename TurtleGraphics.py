from TurtleBase import *
import turtle as t

class TurtleGraphics(TurtleBase):

    def left(self, angle):
        t.left(angle)

    def right(self, angle):
        t.right(angle)

    def forward(self, distance):
        t.forward(distance)

    def position(self):
        return t.position()

    def heading(self):
        return t.heading()

    def setpos(self, pos):
        t.setpos(pos[0], pos[1])

    def seth(self, heading):
        t.seth(heading)

    def penup(self):
        t.penup()

    def pendown(self):
        t.pendown()

    def done(self):
        t.done()