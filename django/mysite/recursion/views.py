from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render
from recursion.Lindenmayer import *
import ast
import os
from random import random
from PIL import Image, ImageDraw
from .models import Fractal


def index(request):
    fractal_list = Fractal.objects.all()
    # create thumbnails
    for fractal in fractal_list:
        filename = "recursion/static/recursion/fractal_img_" + str(fractal.id) + "_thumbnail.png"

        createImage(fractal, filename, fractal.recursion_lvl, fractal.recursion_lvl)

        im = Image.open(filename)
        im.thumbnail((128, 128), Image.ANTIALIAS)
        im.save(filename)

    no_cache = str(int(round(random()*20000, 0))).zfill(5)

    template = loader.get_template('recursion/fractal_list.html')
    context = { "fractal_list" : fractal_list, "no_cache" : no_cache }
    return HttpResponse(template.render(context, request))


def detail(request, fractal_id, recursion_lvl):
    
    filename = "recursion/static/recursion/fractal_img_" + str(fractal_id) + "_" + str(recursion_lvl) + ".png"
    fractal = Fractal.objects.get(pk=fractal_id)
    max_lvl = 15
    
    createImage(fractal, filename, recursion_lvl, max_lvl)

    no_cache = str(int(round(random()*20000, 0))).zfill(5)

    template = loader.get_template('recursion/fractal_result.html')
    context = { "fractal" : fractal, "recursion_lvl" : recursion_lvl, "max_lvl" : max_lvl, "no_cache" : no_cache }
    return HttpResponse(template.render(context, request))

def result(request, fractal_id):
	return HttpResponse("result")

def vote(request, fractal_id):
    return HttpResponse("vote")


#helper 
def createImage(fractal, filename, recursion_lvl, max_lvl):

    if not os.path.isfile(filename):
        turty = TurtleImage((0,0), 0, filename)
        
        lsystems = fractal.lsystem_set.all()
        words = dict()
        for l in lsystems:
        	words[l.lsystem_symbol] = l.lsystem_word

        turty.seth(float(fractal.init_heading))
        pos = ast.literal_eval(fractal.init_pos)
        pos = [float(el) for el in pos]
        # print("pos: " + str(pos))
        turty.setpos(pos)

        recursion_lvl = int(recursion_lvl)
        

        # limit this or server will crash
        max_lvl = int(max_lvl)
        if recursion_lvl > max_lvl:
            recursion_lvl = max_lvl

        lm = Lindenmayer(turty, words, float(fractal.angle), 
        	float(fractal.length), float(fractal.ratio), int(recursion_lvl))

        msg = ""
        if lm is not None:
            # print("start lm")
            lm.start()
            msg = "lm finished"
        else:
            msg = "lm was none"
    else:
        print("requested image already there")

