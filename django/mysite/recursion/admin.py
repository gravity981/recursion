from django.contrib import admin

from .models import Fractal
from .models import LSystem


admin.site.register(Fractal)
admin.site.register(LSystem)
