from django.conf.urls import url

from . import views

app_name = 'recursion'

urlpatterns = [
 
    url(r'^$', views.index, name='index'),
 
    url(r'^(?P<fractal_id>[0-9]+)\/(?P<recursion_lvl>[0-9]+)$', views.detail, name='detail'),

    url(r'^(?P<fractal_id>[0-9]+)/vote/$', views.vote, name='vote'),

    url(r'^(?P<fractal_id>[0-9]+)/result/$', views.result, name='result'),
]
