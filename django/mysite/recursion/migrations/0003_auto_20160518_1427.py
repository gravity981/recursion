# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-18 12:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recursion', '0002_auto_20160518_1424'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lsystem',
            name='lsystem_symbol',
            field=models.CharField(max_length=200),
        ),
    ]
