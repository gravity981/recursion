from django.db import models
import os
from glob import glob

class Fractal(models.Model):
    fractal_name = models.CharField(max_length=200)
    angle = models.CharField(max_length=200)
    length = models.CharField(max_length=200)
    ratio = models.CharField(max_length=200)
    recursion_lvl = models.CharField(max_length=200)
    init_pos = models.CharField(max_length=200)
    init_heading = models.CharField(max_length=200)
    pen_color = models.CharField(max_length=200)
    fill_color = models.CharField(max_length=200)
    

    def __str__(self):             
        return self.fractal_name


    def save(self, *args, **kwargs):
        # print("todo: delete existing fractal images on fractal change")
        for f in glob("recursion/static/recursion/fractal_img_"+str(self.id)+"*.png"):
            os.remove(f)
        super(Fractal, self).save(*args, **kwargs)


class LSystem(models.Model):    
    fractal = models.ForeignKey(Fractal, on_delete=models.CASCADE)
    lsystem_symbol = models.CharField(max_length=200)
    lsystem_word = models.CharField(max_length=200)


    def __str__(self):          
        
        return (self.fractal.fractal_name + ": (" + self.lsystem_symbol + " --> " + self.lsystem_word + ")")


    def save(self, *args, **kwargs):
        # print("todo: delete existing fractal images on lsystem change")
        for f in glob("recursion/static/recursion/fractal_img_"+str(self.fractal.id)+"*.png"):
            os.remove(f)
        super(LSystem, self).save(*args, **kwargs)

