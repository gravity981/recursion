class TurtleBase:
    def __init__(self, pos, head):
        self.pos = pos
        self.head = head

    def left(self, angle):
        raise NotImplementedError()

    def right(self, angle):
        raise NotImplementedError()

    def forward(self, distance):
        raise NotImplementedError()

    def position(self):
        raise NotImplementedError()

    def heading(self):
        raise NotImplementedError()

    def setpos(self, pos):
        raise NotImplementedError()

    def seth(self, heading):
        raise NotImplementedError()

    def penup(self):
        raise NotImplementedError()

    def pendown(self):
        raise NotImplementedError()

    def done(self):
        raise NotImplementedError()

    def pencolor(self, color):
        print("TurtleBase: pencolor not yet implemented")

